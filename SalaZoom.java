import java.util.Arrays;

public class SalaZoom{
  
  private String  id;
  // true: libre, false:ocupado
  private boolean state[][];
  
  public SalaZoom(){}
  
  public SalaZoom( String id ){
    this.id = id;
    this.state = new boolean[6][14];
    for( boolean[] row: state ){
      Arrays.fill( row, true );
    }
  }
  
  public String getId(){
    return id;
  }
  
  public boolean[][] isState(){
    return state;
  }
  
  public void setBusyState( int row, int column ){
    this.state[row][column] = false;
  }
  
  @Override
  public String toString(){
    return "SalaZoom [id=" + id + ", state=" + Arrays.toString( state ) + "]";
  }
  
  public void setBusyState( Horario horario ){
    int dia = horario.getDia();
    int horaInicio = horario.getHoraInicio();
    int duracion = horario.getDuracion();
//  System.out.println( "\tdia -->"+dia );
//  System.out.println( "\thoraDelDia -->"+horaDelDia );

    for( int i = horaInicio; i < duracion + horaInicio - 1; i++ ){
      this.setBusyState( dia, i );
    }
  }
}
