public class Horario{
  
  // Lunes -> 0, Martes ->1 ...
  private int    dia;
  private int    horaInicio;
  private int    duracion;
  private String salaZoomId;
  
  public Horario(){}
  
  public Horario( int dia, int horaInicio, int duracion ){
    this.dia = dia;
    this.horaInicio = horaInicio;
    this.duracion = duracion;
  }
  
  public int getDia(){
    return dia;
  }
  
  public int getHoraInicio(){
    return horaInicio;
  }
  
  public int getDuracion(){
    return duracion;
  }
  
  public String getSalaZoomId(){
    return salaZoomId;
  }
  
  public void setSalaZoomId( String id ){
    this.salaZoomId = id;
  }

  @Override
  public String toString(){
    return "Horario [dia=" + dia + ", horaInicio=" + horaInicio + ", duracion=" + duracion + ", salaZoomId="
        + salaZoomId + "]";
  }
  
}
