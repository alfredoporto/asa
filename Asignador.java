import java.util.ArrayList;

public class Asignador {
    private ArrayList<Curso> cursos;
    private ArrayList<SalaZoom> salasZoom;

    public Asignador() {
        this.cursos = new ArrayList<Curso>();
        this.salasZoom = new ArrayList<SalaZoom>();
    }

    public Asignador(ArrayList<Curso> cursos, ArrayList<SalaZoom> salasZoom) {
        this.cursos = cursos;
        this.salasZoom = salasZoom;
    }

    public void asignarSalasZoom() {

        cursos.forEach((curso) -> {
          System.out.println( "\tCurso actual-->"+curso.getCod() ); 
            curso.getHorarios().forEach((horario) -> {
              System.out.println( "\tHorario revisado-->"+horario.toString() ); 
                
                this.salasZoom.stream()
                              .forEach( sala->{
                                System.out.println( "\tSala Zoom actual-->"+sala );
                                
                                boolean esPosibleAsignarSalaZoom = getPosibleAsignacionSalaZoom( horario,
                                    sala );
                                asignandoSalaZoom( horario, sala, esPosibleAsignarSalaZoom );
                              } );
                              
            });
        });
    }

    private boolean getPosibleAsignacionSalaZoom( Horario horario, SalaZoom sala ){
      boolean esPosibleAsignarSalaZoom=true;
      int dia = horario.getDia();
      int horaInicio = horario.getHoraInicio();
      int duracion = horario.getDuracion();
      for (int horaDelDia = horaInicio; horaDelDia < duracion + horaInicio; horaDelDia++) {
        if (!sala.isState()[dia][horaDelDia] ) {
          esPosibleAsignarSalaZoom=false;
          break;
        }
               }
      return esPosibleAsignarSalaZoom;
    }

    private void asignandoSalaZoom( Horario horario, SalaZoom salaZoom, boolean esPosibleAsignarSalaZoom ){
      if( esPosibleAsignarSalaZoom && horario.getSalaZoomId()==null){
        horario.setSalaZoomId(salaZoom.getId());
        salaZoom.setBusyState(horario);
        System.out.println( "\t sala Zoom cargado");
      }
    }


    public static void main(String[] args) {
        ArrayList<Curso> cursosTest = new ArrayList<Curso>();
        Curso curso1 = new Curso("ST-01");
        Curso curso2 = new Curso("ST-02");
        
        Curso curso3 = new Curso("ST-03");
        /*
        Curso curso4 = new Curso("ST-04");
        Curso curso5 = new Curso("ST-05");*/

        curso1.addHorario(new Horario(0, 0, 2));
        curso1.addHorario(new Horario(2, 0, 2));

        curso2.addHorario(new Horario(0, 0, 2));
        curso2.addHorario(new Horario(2, 0, 2));

        curso3.addHorario(new Horario(0, 0, 2));
        curso3.addHorario(new Horario(2, 0, 2));
        /*
        curso4.addHorario(new Horario(2, 0, 2));
        curso4.addHorario(new Horario(4, 0, 2));

        curso5.addHorario(new Horario(1, 0, 2));
        curso5.addHorario(new Horario(5, 0, 2));
*/
        cursosTest.add(curso1);
        cursosTest.add(curso2);
       
        cursosTest.add(curso3);
        /*
        cursosTest.add(curso4);
        cursosTest.add(curso5);
         */

        ArrayList<SalaZoom> salaZoomsTest = new ArrayList<SalaZoom>();
        SalaZoom salaZoom1 = new SalaZoom("A01");
        SalaZoom salaZoom2 = new SalaZoom("A02");

        salaZoomsTest.add(salaZoom1);
        salaZoomsTest.add(salaZoom2);

        System.out.println("Salas zoom al inicio");
        System.out.println(" salaZoom [0] en [0][0] "+salaZoomsTest.get(0).isState()[0][0]);
        System.out.println(" salaZoom [1] en [0][0] "+salaZoomsTest.get(1).isState()[0][0]);
        Asignador asignadorTest = new Asignador(cursosTest, salaZoomsTest);
        asignadorTest.asignarSalasZoom();

        cursosTest.forEach((curso) -> {
            System.out.println(curso.getCod());
            curso.getHorarios().forEach((horario) -> {
                System.out.println(horario.getDia());
                System.out.println(horario.getHoraInicio());
                System.out.println(horario.getSalaZoomId());
            });
        });
        System.out.println("Salas zoom al fin");
        System.out.println(" salaZoom [0] en [0][0] "+salaZoomsTest.get(0).isState()[0][0]);
        System.out.println(" salaZoom [1] en [0][0] "+salaZoomsTest.get(1).isState()[0][0]);
    }
}
