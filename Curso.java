import java.util.ArrayList;

public class Curso{
  
  private String             cod;
  private ArrayList<Horario> horarios;
  
  public Curso(){}
  
  public Curso( String cod ){
    this.cod = cod;
    this.horarios = new ArrayList<Horario>();
  }
  
  public String getCod(){
    return cod;
  }
  
  public ArrayList<Horario> getHorarios(){
    return horarios;
  }
  
  public void addHorario( Horario horario ){
    this.horarios.add( horario );
  }
  
  @Override
  public String toString(){
    return "Curso [cod=" + cod + ", horarios=" + horarios + "]";
  }
}
